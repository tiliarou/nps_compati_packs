require 'git'
require 'csv'
require 'open-uri'
require 'pry'
require 'logger'
require 'twitter'
require 'active_support/all'
g = Git.open(".")
untrackeds = g.status.untracked
changeds = g.status.changed

tries = 5
begin
  games = CSV.parse(open("http://beta.nopaystation.com/tsv/PSV_GAMES.tsv"), { :col_sep => "\t", :headers => true, encoding: "ISO8859-1"})
  demos = CSV.parse(open("http://beta.nopaystation.com/tsv/PSV_DEMOS.tsv"), { :col_sep => "\t", :headers => true, encoding: "ISO8859-1"})
rescue
  tries -= 1
  sleep 5
  retry if tries != 0
  exit
end


filtered = {}
games.map{|i| filtered[i["Title ID"]] = { "Name": i["Name"], "Region": i["Region"] } }
demos.map{|i| filtered[i["Title ID"]] = { "Name": i["Name"], "Region": i["Region"] } }

new_packs = changeds.merge!(untrackeds) { |key, changed, untracked| changed }


if not new_packs.empty? 
  result = ""
  new_packs.each do |filename, git_file|
    if not filename.end_with? ".ppk"
      next
    end

    if filename.start_with? "patch/"
      game_title_id = filename.split("/")[1]
      version = filename.split("/")[2].split("-")[2].gsub("_",".").to_f
      type = "- [P] "
    else
      game_title_id = filename.split("/")[0]
      version = filename.split("/")[1].split("-")[2].gsub("_",".").to_f
      type = "- [B] "
    end

    game_name = filtered[game_title_id][:Name]
    game_region = filtered[game_title_id][:Region]

    result += "#{type} #{game_name.gsub("*","")} [#{game_region}] ver #{version} \n"
  end

  client = Twitter::REST::Client.new do |config|
    config.consumer_key        = ENV["twitter_consumer_key"]
    config.consumer_secret     = ENV["twitter_consumer_secret"]
    config.access_token        = ENV["twitter_access_token"]
    config.access_token_secret = ENV["twitter_access_token_secret"]
  end


  if (result.size / 230.0) > 1
    result_split = result.split("\n")
    current_id = nil
    (result.size/230.0).ceil.times do
      tweet = "New Comp Packs:\n"
      tweet += result_split.shift(3).join("\n")
      tweet += "\n#nopaystation\n"
      Time.zone = "Europe/London"
      tweet += "#{Time.zone.now.to_i}\n"

      puts tweet
      puts current_id
      if not current_id.nil?
        current_id = client.update(tweet, in_reply_to_status_id: current_id).id
      else
        current_id = client.update(tweet).id
      end

      if result.size == 1
        tweet = "New Comp Packs:\n"
        tweet += result_split.shift(1).join("\n")
        tweet += "\n#nopaystation\n"
        Time.zone = "Europe/London"
        tweet += "#{Time.zone.now.to_i}\n"
        puts tweet
        current_id = client.update(tweet, in_reply_to_status_id: current_id)
      end
    end




  else
    tweet = "New Comp Packs:\n"
    tweet += result

    tweet += "#nopaystation\n"

    Time.zone = "Europe/London"
    tweet += "#{Time.zone.now.to_i}\n"

    client = Twitter::REST::Client.new do |config|
      config.consumer_key        = ENV["twitter_consumer_key"]
      config.consumer_secret     = ENV["twitter_consumer_secret"]
      config.access_token        = ENV["twitter_access_token"]
      config.access_token_secret = ENV["twitter_access_token_secret"]
    end

    client.update(tweet)
  end
end
